package com.compario.qrdecoder

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.compario.compario_sdk.ComparioQRValidationResponse
import com.compario.compario_sdk.ErrorResponse
import com.compario.compario_sdk.OnRespondReceive
import com.compario.compario_sdk.QrDecoder

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        QrDecoder.setBaseUrl("https://0p6s3jvzu9.execute-api.ap-northeast-1.amazonaws.com/prod/ride/")
        QrDecoder.startScanner(this, "eyJraWQiOiJJeE1wbXZROW1nMVB3M3hadGwyK1dRQVVkbjh2NW5wcnYyWVBybFdQZVBZPSIsImFsZyI6IlJTMjU2In0.eyJzdWIiOiIxYTYxNjk4Yi1mZTE0LTRhMTItOGU5Yi00ZmFiMDExYjVlNjQiLCJhdWQiOiI3MTJ1cjkyc2N2YzNxMjhhdWdqMDZuaXYzMSIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJldmVudF9pZCI6ImUyNWVjYTUzLWIwZjEtMTFlOC1hNjhlLTY5ZjEwNTc0OTYyMCIsInRva2VuX3VzZSI6ImlkIiwiYXV0aF90aW1lIjoxNTM2MTQxMzU2LCJpc3MiOiJodHRwczpcL1wvY29nbml0by1pZHAuYXAtbm9ydGhlYXN0LTEuYW1hem9uYXdzLmNvbVwvYXAtbm9ydGhlYXN0LTFfclJ2MjBsbXo5IiwiY29nbml0bzp1c2VybmFtZSI6InpuYXZpbS1hdC1nbWFpbC5jb20iLCJleHAiOjE1MzYxNzIzMzQsImlhdCI6MTUzNjE2ODczNCwiZW1haWwiOiJ6bmF2aW1AZ21haWwuY29tIn0.Bo_3_wrKG7AF3Xfj3bVr7xuk5KowVOEDmlDrDlx4Ao1GtN92wj4IWXyeGUsw_aDUXfLgjPM7XHzShnlK3xJe_ybgyNfxWvujQud3zoEl1MgluoHcAfGz4LkaH8X6AzE3_ryrxXXL9O64nzXqBbkz3l6dCDMFEzpni31PU9qcbJUC8suTX61xnl3FcSO0qg7Q81odvLcxpMkM9WecpvJZbMtuLLNprUrhG-WdyAqwT4p-biI6VwhFfaznqY11jdkGnim7MJ34qz-zJBu3zsMXQLkHIAXLZAQG2ivKcY7a8CKX-_r4WEJNj4h-W0d1PKuCfTjwKgJyIgff6ggYHODKGg", object: OnRespondReceive {
            override fun onValidationStart() {
                // optionally trigger loading prompt
            }

            override fun onSuccess(response: ComparioQRValidationResponse?) {
                // handle response
                Toast.makeText(applicationContext, response?.isValid.toString(), Toast.LENGTH_LONG).show()
            }

            override fun onError(error: ErrorResponse?) {
                // handle error
                Toast.makeText(applicationContext, "Error: " + error?.errorMessage, Toast.LENGTH_LONG).show()
            }
        })


    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        QrDecoder.onActivityResult(requestCode, resultCode, data)
    }

    override fun onDestroy() {
        super.onDestroy()
        QrDecoder.onDestroy()
    }
}
